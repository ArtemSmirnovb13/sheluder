import pymysql
import pymysql.cursors
import datetime
import math

con = pymysql.connect('localhost', 'root', '', 'python')
cur = con.cursor()
def Calls():
    with con:
        for i in range(24):
            if i == 23:
                date_one = datetime.datetime(2020, 5, datetime.datetime.today().day, i, 00, 00)
                date_two = datetime.datetime(2020, 5, datetime.datetime.today().day + 1, 00, 00, 00)
            else:
                date_one = datetime.datetime(2020, 5, datetime.datetime.today().day, i, 00, 00)
                date_two = datetime.datetime(2020, 5, datetime.datetime.today().day, i + 1, 00, 00)

            date = datetime.datetime.today().strftime("%Y-%m-%d")
            day = datetime.datetime.today().strftime("%A")
            cur = con.cursor()
            sql = "SELECT * FROM calls where star_call BETWEEN %s AND %s"
            cur.execute(sql, (date_one, date_two))
            rows = cur.fetchall()
            sum = 0
            for x in range(len(rows)):
                sum += rows[x][2]
            try:
                aht = math.floor(sum / len(rows))
            except Exception as e:
                aht = 0
            sql = "INSERT INTO `call_stat_per_day` (`hour`, `date`,`day`,`call_sum`,`sum_sec`, `aht`) VALUES (%s, %s,%s,%s, %s, %s)"
            cur.execute(sql, (i, date, day, len(rows), sum, aht))  # test migration

