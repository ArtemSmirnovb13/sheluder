from tkinter import *
from tkinter import messagebox
import Classes
import User
import datetime
import Admin

print(datetime.datetime.minute)
connect = Classes.Connect('localhost', 'root', '', 'python')
cur = connect.connection()


def main(curr_user):
    check_is_admin(curr_user)
    if check_is_admin(curr_user) == 1:
        Admin.main()
    elif check_is_admin(curr_user) == 0:
        User.work(curr_user)



def check_is_admin(curr_user):
    print(curr_user)
    if curr_user[6] == 1:
        return TRUE
    else:
        return FALSE


def auth():
    sql = "SELECT * FROM Users WHERE login = %s AND password = %s"
    cur.execute(sql, (name.get(), surname.get()))
    current_user = cur.fetchone()
    if current_user != None:
        messagebox.showinfo("Good", current_user)
        root.destroy()
        main(current_user)
    else:
        messagebox.showinfo("BAD", 'Пользователь не найден')


root = Tk()
root.title("GUI на Python")

name = StringVar()
surname = StringVar()

name_label = Label(text="Введите логин:")
surname_label = Label(text="Введите пароль:")

name_label.grid(row=0, column=0, sticky="w")
surname_label.grid(row=1, column=0, sticky="w")

name_entry = Entry(textvariable=name)
surname_entry = Entry(textvariable=surname)

name_entry.grid(row=0, column=1, padx=5, pady=5)
surname_entry.grid(row=1, column=1, padx=5, pady=5)

message_button = Button(text="Click Me", command=auth)
message_button.grid(row=2, column=1, padx=5, pady=5, sticky="e")

root.mainloop()
