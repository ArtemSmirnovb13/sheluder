from datetime import date, timedelta
from calendar import _monthlen
from openpyxl import load_workbook, Workbook
from copy import copy
from operator import add, mul
from functools import reduce


class Graph(object):
    class Shift(object):
        __shifts__ = {
            # 4 часа
            '00/04': [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '08/12': [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '09/13': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '10/14': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '11/15': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '12/16': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '13/17': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '14/18': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '15/19': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '16/20': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '17/21': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '18/22': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '19/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '20/00': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '21/01': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '22/02': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            '23/03': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
            # 5 часов
            '18/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            # 6 часов
            '09/15': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '11/17': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            # 7 часов
            '09/17': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '10/18': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '12/20': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '13/21': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '14/22': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '16/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '17/00': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '18/01': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '19/02': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            # 8 часов
            '00/09': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '07/16': [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '08/17': [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '09/18': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '10/19': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '11/20': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '12/21': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '13/22': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '14/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '15/00': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '16/01': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '17/02': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            '18/03': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
            '19/04': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
            '20/05': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
            '21/06': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
            '22/07': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
            '23/08': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            '09/19': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            # 9 часов
            '13/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '17/03': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
            # 10 часов
            '10/21': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '21/08': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            # 11 часов
            '08/20': [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '09/21': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '10/22': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '11/23': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '12/00': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '13/01': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            '14/02': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            '21/09': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        }

        def __init__(self, s_date, s_symbol=None, s_lunch=1):
            self.s_date = s_date
            self.s_symbol = s_symbol
            self.s_lunch = s_lunch

        def __getitem__(self, item):
            if not isinstance(item, str):
                raise TypeError
            shift_template = copy(self.__shifts__[item])
            if 8 < sum(shift_template) < 12:
                shift_template[shift_template.index(1) + 2 + self.s_lunch] = 0
            elif sum(shift_template) == 12:
                from_index = shift_template.index(1)
                to_index = shift_template.index(0, from_index)
                a = 40 * (self.s_lunch - 1)
                b = 40 + a
                if a // 60 != b // 60 and b % 60 != 0:
                    shift_template[from_index + 3 + a // 60] = 2/3
                    shift_template[from_index + 3 + b // 60] = 2/3
                else:
                    shift_template[from_index + 3 + a // 60] = 1/3
                c = 20 * (self.s_lunch - 1)
                shift_template[to_index - 5 + c // 60] = 2/3
            return shift_template

        def __str__(self):
            return self.s_symbol if self.s_symbol is not None else ''

        def work_vector(self):
            if self.s_symbol is None or len(self.s_symbol) != 5:
                return [0] * 34
            return self[self.s_symbol]

        def __eq__(self, other):
            if isinstance(other, Graph.Shift):
                return self.s_symbol == other.s_symbol and self.s_lunch == other.s_lunch and self.s_date == other.s_date
            return False

        def __hash__(self):
            return hash((self.s_symbol, self.s_lunch, self.s_date))

        def break_time(self):
            from datetime import datetime

            if self.s_symbol == '21/09':
                return []
            shift_template = copy(self.__shifts__[self.s_symbol])
            if 8 < sum(shift_template) < 12 or self.s_symbol == '09/17':
                t = datetime(self.s_date.year, self.s_date.month, self.s_date.day, shift_template.index(1) + 2 + self.s_lunch, 0)
                f = t + timedelta(hours=1)
                return [(t.time(), f.time())]
            elif sum(shift_template) == 12:
                t = datetime(self.s_date.year, self.s_date.month, self.s_date.day, shift_template.index(1) + 3, 0)
                t += timedelta(minutes=40 * (self.s_lunch - 1))
                f = t + timedelta(minutes=40)
                t2 = datetime(self.s_date.year, self.s_date.month, self.s_date.day, shift_template.index(0, shift_template.index(1)) - 5, 0)
                t2 += timedelta(minutes=20 * (self.s_lunch - 1))
                f2 = t2 + timedelta(minutes=20)
                return [(t.time(), f.time()), (t2.time(), f2.time())]
            else:
                return []

    def __init__(self, graph_file, start_cell, start_date: date, forecast_start_cell=None, aht_start_cell=None):
        super(Graph, self).__init__()
        self.graph_file = graph_file
        self.start_cell = start_cell
        self.start_date = start_date
        self.forecast_start_cell = forecast_start_cell
        self.aht_start_cell = aht_start_cell
        self.graphs = {}
        self.forecast = {}
        self.aht = {}
        self.__negative_multiplicators__ = 1
        self.__people_matrix__ = {}
        self.__load__()

    def __load__(self):
        wb = load_workbook(self.graph_file, data_only= True)
        ws = wb.active
        row = ws[self.start_cell].row
        col = ws[self.start_cell].col_idx
        while ws.cell(row, 3).value is not None:
            name = ws.cell(row, 3).value
            shifts = [Graph.Shift(self.start_date + timedelta(days=day), ws.cell(row, col+day).value)
                      for day in range(-1, _monthlen(self.start_date.year, self.start_date.month))]
            self.graphs[name] = shifts
            row += 1
        if self.forecast_start_cell is not None:
            start_row = ws[self.forecast_start_cell].row
            start_col = ws[self.forecast_start_cell].col_idx
            print(self.start_date.year, self.start_date.month)
            for col in range(_monthlen(self.start_date.year, self.start_date.month)):
                self.forecast[self.start_date + timedelta(days=col)] = [ws.cell(row + start_row, start_col + col).value for row in range(24)]
            last_dt, last_forecast = max(self.forecast.items())
            self.forecast[last_dt + timedelta(days=1)] = last_forecast
        if self.aht_start_cell is not None:
            start_row = ws[self.aht_start_cell].row
            start_col = ws[self.aht_start_cell].col_idx
            for col in range(_monthlen(self.start_date.year, self.start_date.month)):
                self.aht[self.start_date + timedelta(days=col)] = [ws.cell(row + start_row, start_col + col).value for row in range(24)]
            last_dt, last_aht = max(self.aht.items())
            self.aht[last_dt + timedelta(days=1)] = last_aht
        wb.close()

    @property
    def negative_multiplicators(self):
        return self.__negative_multiplicators__

    @negative_multiplicators.setter
    def negative_multiplicators(self, value):
        self.__negative_multiplicators__ = reduce(mul, value.values(), 1)

    def deal(self, dt, external_shifts=None):
        forecast_vector = self.forecast_vector(dt)
        return sum(self.calls_per_hour(dt, self.people_per_hour(dt, external_shifts), forecast_vector)) / sum(forecast_vector)

    def forecast_vector(self, dt):
        return self.forecast[dt] + self.forecast[dt + timedelta(days=1)][:4]

    def calls_per_hour(self, dt, people_vector, forecast_vector):
        F = forecast_vector
        T = self.aht[dt] + self.aht[dt + timedelta(days=1)][:4]
        C = people_vector[:24 + 4]
        D = []

        m = self.negative_multiplicators
        for i in range(len(F)):

            d = (C[i] * (3600 / T[i]) * m)

            d = F[i] if d > float(F[i]) else d
            D.append(d)
        return D

    def people_per_hour(self, dt, external_shifts=None):
        if dt not in self.__people_matrix__:
            m = copy(Graph.Shift(dt).work_vector())
            for shifts in self.graphs.values():
                last_day_shift = shifts[dt.day - 1]
                m = list(map(add, m, last_day_shift.work_vector()[-10:] + [0] * 24))
                shift = shifts[dt.day]
                m = list(map(add, m, shift.work_vector()))
            self.__people_matrix__[dt] = m
        base_vector = copy(self.__people_matrix__[dt])
        if external_shifts is not None:
            for shift in external_shifts:
                if not isinstance(shift, Graph.Shift):
                    shift = Graph.Shift(dt, shift[0], shift[1])
                base_vector = list(map(add, base_vector, shift.work_vector()))
        return base_vector

    def append_shifts(self, dt, replace, shifts):
        shifts = sorted(shifts, key=lambda s: str(s))
        available = filter(lambda x: x[1][dt.day].s_symbol == replace, self.graphs.items())
        available = sorted(available, key=lambda x: (str(x[1][dt.day-1]), str(x[1][dt.day])))
        for index, shift in enumerate(shifts):
            available[index][1][dt.day] = shift

    def save_graph(self):
        wb = Workbook()
        ws = wb.active

        for row, (name, graphs) in enumerate(self.graphs.items()):
            ws.cell(row + 1, 1, name)
            for col, shift in enumerate(map(str, graphs)):
                ws.cell(row + 1, 2+col, shift)
        wb.save(r'calculate.xlsx')

