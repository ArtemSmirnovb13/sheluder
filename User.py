from time import monotonic as now
from tkinter import Tk, Button
from tkinter import *
import Classes
import datetime
import pymysql

con = pymysql.connect('localhost', 'root', '', 'python')
cur = con.cursor()


def work(user):
    def end_call():
        try:
            start = datetime.datetime.strptime(label1['text'], '%Y-%m-%d %H:%M:%S.%f')
            end = datetime.datetime.now()
            call_seconds = (end - start).seconds
            with con:
                sql = "INSERT INTO `Calls` (`user_id`,`sec`, `star_call`, end_call) VALUES (%s, %s, %s, %s)"
                cur.execute(sql, (user[0], call_seconds, label1['text'], datetime.datetime.now()))  # test migration
        except Exception as e:
            print("Exeception occured:{}".format(e))
        finally:
            con.close()

    def timer():
        label1['text'] = datetime.datetime.now()

    def exit():
        try:
            start_shift = start_work_time
            end_shift = datetime.datetime.now()
            #СЕКУНДЫ!!!
            work_shift = (end_shift - start_shift).seconds
            with con:
                cur.execute("SELECT shift FROM users WHERE `id` = %s", (user[0]))
                prev_shift = int(cur.fetchone()[0])
                prev_shift += work_shift
                sql_users = "UPDATE Users SET shift = %s WHERE id = %s"
                cur.execute(sql_users, (prev_shift, user[0]))
                root.destroy()
        except Exception as e:
            print("Exeception occured:{}".format(e))
        finally:
            con.close()


    root = Tk()
    button = Button(root, text="звонок принят", command=timer)
    button.pack()
    button2 = Button(root, text="звонок завершён", command=end_call)
    button2.pack()
    button_exit = Button(root, text="Конец смены", command=exit)
    button_exit.pack()
    root.eval('tk::PlaceWindow %s center' % root.winfo_pathname(root.winfo_id()))
    root.geometry("300x250")
    label1 = Label(text="", fg="#eee", justify=LEFT)
    label_time = Label(text='{0}{1}'.format('Смена началась : ', datetime.datetime.now()), fg="#eee", bg="#333",
                       justify=LEFT)
    start_work_time = datetime.datetime.now()
    label_user_id = Label(text="ID сотрудника " + str(user[0]), fg="#eee", bg="#333", justify=LEFT)
    label1.place(relx=.2, rely=.6)
    label1.pack()
    label_time.pack()
    label_user_id.pack()
    root.mainloop()
