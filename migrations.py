import pymysql
import pymysql.cursors

con = pymysql.connect('localhost', 'root',
                      '', 'python')

cur = con.cursor()
array = [
    "CREATE TABLE Users(id int AUTO_INCREMENT NOT NULL, email TEXT,login TEXT, name TEXT UNIQUE, password TEXT, shift int, admin int, PRIMARY KEY (`id`))",
    "CREATE TABLE Calls(id int AUTO_INCREMENT NOT NULL, user_id int,sec int,star_call timestamp, end_call timestamp, PRIMARY KEY (`id`))",
    "CREATE TABLE call_stat_per_day(id int AUTO_INCREMENT NOT NULL, hour int,date timestamp,day TEXT,call_sum int, sum_sec int, aht int, PRIMARY KEY (`id`))",
]

try:
    with con:
        for i in array:
            try:
                cur.execute(i)
            except Exception as e:
                print("Exeception occured:{}".format(e))

        sql = "INSERT INTO `users` (`email`, `name`,`login`, `password`,`shift`, `admin`) VALUES (%s, %s,%s, %s, %s, %s)"
        cur.execute(sql, ('test@test', 'sveta', 'root', '123', '160', '0'))  # test migration
except Exception as e:

    print("Exeception occured:{}".format(e))

finally:
    con.close()
