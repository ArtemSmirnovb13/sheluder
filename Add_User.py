from tkinter import *
from tkinter import messagebox
from Classes import User



def work():
    def Add():
        user = User(email_entry.get(), login_entry.get(), name_entry.get(), pass_entry.get(), admin_entry.get())
        user.Add()

    add_user = Tk()
    add_user.title("GUI на Python")

    email = StringVar()
    login = StringVar()
    name = StringVar()
    password = StringVar()
    shift = 0
    admin = IntVar()

    label1 = Label(add_user, text="Введите имейл:")
    label2 = Label(add_user, text="Введите логин:")
    label3 = Label(add_user, text="Введите имя:")
    label4 = Label(add_user, text="Введите пароль:")
    label5 = Label(add_user, text="Введите права:")

    label1.grid(row=0, column=0, sticky="w")
    label2.grid(row=1, column=0, sticky="w")
    label3.grid(row=2, column=0, sticky="w")
    label4.grid(row=3, column=0, sticky="w")
    label5.grid(row=4, column=0, sticky="w")

    email_entry = Entry(add_user, textvariable=email)
    login_entry = Entry(add_user, textvariable=login)
    name_entry = Entry(add_user, textvariable=name)
    pass_entry = Entry(add_user, textvariable=password)
    admin_entry = Entry(add_user, textvariable=admin)

    email_entry.grid(row=0, column=1, padx=5, pady=5)
    login_entry.grid(row=1, column=1, padx=5, pady=5)
    name_entry.grid(row=2, column=1, padx=5, pady=5)
    pass_entry.grid(row=3, column=1, padx=5, pady=5)
    admin_entry.grid(row=4, column=1, padx=5, pady=5)

    message_button = Button(add_user, text="Click Me", command=Add)
    message_button.grid(row=5, column=2, padx=5, pady=5, sticky="e")

    add_user.mainloop()




