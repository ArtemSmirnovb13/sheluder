import numpy as np
from itertools import combinations
from copy import copy
import random
from time import process_time

class GeneticShiftLaunch(object):
    def __init__(self, shift_set, launch_range, mutation_rate, target_func):
        self.shift_set = shift_set
        self.launch_range = launch_range
        self.population = []
        self.mutation_rate = mutation_rate
        self.target_func = target_func
        self.data = []

    def generate_init_population(self, population_size):
        launch_from, launch_to = self.launch_range
        a = np.random.randint(0, len(self.shift_set), (population_size, 2))
        b = np.random.randint(launch_from, launch_to, (population_size, 2))
        a *= (1, 0)
        b *= (0, 1)
        self.population = (a + b).tolist()

    def fitness(self, people_count, steps=15000):

        self.generate_init_population(people_count * 2)

        for i in range(steps):
            #s = process_time()
            self.genocide(people_count)
            print(people_count)
            for x, y in combinations(self.population, 2):
                child = self.crossover(x, y)
                child = self.mutable(child)
                self.population.append(child)
                print(child)
            #e = process_time()
            #print('Шаг', i)

    def genocide(self, people_count):
        best = {}
        for i in range(50):
            p = random.sample(self.population, people_count)
            best[self.target_func(map(lambda x: (self.shift_set[x[0]], x[1]), p))] = p

        res, self.population = max(best.items())
        self.data.append((res, copy(self.population)))

    @staticmethod
    def crossover(x, y, rate=0.65):
        a = x[0] if np.random.rand() > rate else y[0]
        b = x[1] if np.random.rand() > rate else y[1]
        return [a, b]

    def mutable(self, g):
        if np.random.rand() < self.mutation_rate:
            g[0] = np.random.randint(0, len(self.shift_set))
            g[1] = np.random.randint(*self.launch_range)
        return g


