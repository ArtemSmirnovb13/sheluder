from tkinter import *
from tkinter import messagebox
import Classes
import datetime
import pymysql
import Calls_report
import Add_User
import Report


def main():
    def report():
        try:
            Report.work()
            messagebox.showinfo("", 'Отчет сформирован')
        except Exception as e:
            messagebox.showinfo("", "Exeception occured:{}".format(e))
    def config():
        try:
            Calls_report.Calls()
            messagebox.showinfo("", 'Отчет сформирован')
        except Exception as e:
            messagebox.showinfo("", "Exeception occured:{}".format(e))

    def exit():
        root.destroy()

    def user():
        Add_User.work()
    def user_update():
        Classes.Update()

    root = Tk()
    button = Button(root, text="Сформировать", command=config)
    button.pack()

    button_report = Button(root, text="Сформировать эксель файл", command=report)
    button_report.pack()

    main_menu = Menu()

    file_menu = Menu()
    file_menu.add_command(label="Звонки", command=config)
    file_menu.add_command(label="Выход", command=exit)
    main_menu.add_cascade(label="Отчет", menu=file_menu)

    users_menu = Menu()
    users_menu.add_command(label="Добавить пользователя", command=user)
    users_menu.add_command(label="Редактировать пользователя", command=user_update)
    #users_menu.add_command(label="Удалить пользователя", command=config)
    main_menu.add_cascade(label="Пользователи", menu=users_menu)

    root.config(menu=main_menu)
    root.eval('tk::PlaceWindow %s center' % root.winfo_pathname(root.winfo_id()))
    root.geometry("300x250")
    root.mainloop()
