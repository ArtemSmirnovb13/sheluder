from graph import Graph, date
from GeneticShiftLaunch import GeneticShiftLaunch
import pickle
import os
import re


def load_from_dat():
    res = {}
    for file_name in [f for f in os.listdir('.') if re.match(r'.*\.dat', f)]:
        with open(file_name, 'rb') as file:
            shifts = pickle.load(file)
            res[date.fromisoformat(re.search(r'(?P<date>[\d\-]+)\.dat', file_name).group('date'))] = shifts
    return res


if __name__ == '__main__':
    g = Graph(r'Я_График Июнь_fix.xlsm', start_cell='K2', start_date=date(2019, 7, 1), forecast_start_cell='K311', aht_start_cell='K366')
    g.negative_multiplicators = {'sick': 0.93, 'useful_time': 0.925}
    from_dat = False
    if from_dat:
        data = load_from_dat()
    for dt in (date(2019, 7, i+1) for i in range(29)):
        if from_dat:
            g.append_shifts(dt, 'о', data[dt])
            continue
        genetic = GeneticShiftLaunch(['08/20', '09/21', '10/22', '11/23', '12/00', '13/01', '14/02'], (1, 4), 0.05, lambda x: g.deal(dt, x))

        genetic.fitness(int(len([shifts[dt.day] for shifts in g.graphs.values() if shifts[dt.day].s_symbol == 'о'])),850)
        deal, best_pop = max(genetic.data, key=lambda x: x[0])

        print(dt.isoformat(), deal)
        shifts = list(map(lambda x: Graph.Shift(dt, genetic.shift_set[x[0]], x[1]), best_pop))
        g.append_shifts(dt, 'о', shifts)
        with open(dt.isoformat()+'.dat', 'wb') as file:
            pickle.dump(shifts, file)
        # for shift in shifts:
        #     shift.upload_to_spk()
        del genetic
    # g.upload_lunch()

    g.save_graph()


